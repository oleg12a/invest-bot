package com.example.stockapiapplication.service;

import com.example.stockapiapplication.dto.StocksDto;
import com.example.stockapiapplication.dto.TickersDto;
import com.example.stockapiapplication.model.Stock;

public interface StockService {
    Stock getStockByTicker(String ticker);
    StocksDto getStocksByTickers(TickersDto tickers);
}
