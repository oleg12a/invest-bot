package com.example.stockapiapplication.controller;

import com.example.stockapiapplication.dto.StocksDto;
import com.example.stockapiapplication.dto.TickersDto;
import com.example.stockapiapplication.model.Stock;
import com.example.stockapiapplication.service.StockService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class StockController {
    private final StockService stockService;

    @GetMapping("/stocks/{ticker}")
    public Stock getStock(@PathVariable String ticker) {
        return stockService.getStockByTicker(ticker);

    }

    @PostMapping("/stocks/getStocksByTickers")
    public StocksDto getStocksByTickers(@RequestBody TickersDto tickersDto) {
        return stockService.getStocksByTickers(tickersDto);
    }
}
