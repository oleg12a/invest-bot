package com.example.stockapiapplication.dto;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InstrumentDTO {
    private String figi;
    private String ticker;
    private String isin;
    private String currency;
}
