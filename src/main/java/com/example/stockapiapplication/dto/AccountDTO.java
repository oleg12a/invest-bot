package com.example.stockapiapplication.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AccountDTO {
    private String accountId;
    private String brokerAccountType;
    private String getBrokerAccountId;
    private String currency;
}
