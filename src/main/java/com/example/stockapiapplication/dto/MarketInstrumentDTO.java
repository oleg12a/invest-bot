package com.example.stockapiapplication.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MarketInstrumentDTO {
    private String figi;
    private String ticker;
    private String isin;
    private String currency;
    private String name;
    private String type;
}
