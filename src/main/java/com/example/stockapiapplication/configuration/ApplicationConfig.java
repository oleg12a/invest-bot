package com.example.stockapiapplication.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.tinkoff.piapi.core.InvestApi;


@Configuration
@EnableConfigurationProperties(ApiConfig.class)
@RequiredArgsConstructor
public class ApplicationConfig {
    private final ApiConfig apiConfig;

    @Bean
    public InvestApi api() {
        var token = apiConfig.getToken();

        var api = InvestApi.create(token);
        var channel = api.getSandboxService();
        return InvestApi.createSandbox(channel.openAccountSync());

    }
}
