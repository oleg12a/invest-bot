package com.example.stockapiapplication.model;

import lombok.*;

import java.util.Objects;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private String accountId;
    private String brokerAccountType;
    private Currency currency;
    private String getBrokerAccountId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(accountId, account.accountId) && Objects.equals(brokerAccountType, account.brokerAccountType) && currency == account.currency && Objects.equals(getBrokerAccountId, account.getBrokerAccountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, brokerAccountType, currency, getBrokerAccountId);
    }
}
