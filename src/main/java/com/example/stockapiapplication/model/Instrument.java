package com.example.stockapiapplication.model;

import lombok.*;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Instrument {
    private Long id;

    private String figi;

    private String isin;

    private String ticker;

    private Currency currency;

    private String name;

    private BigDecimal increment;

    private int lot;
}
