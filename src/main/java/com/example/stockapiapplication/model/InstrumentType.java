package com.example.stockapiapplication.model;

public enum InstrumentType {
    STOCK, ETF, BOND, CURRENCY;
}
