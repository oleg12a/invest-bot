package com.example.stockapiapplication.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

public class Operation {
    private String id;
    private String status;
    private OperationType operationType;
    private Currency currency;
    private BigDecimal payment;
    private BigDecimal commission;
    private String figi;
    private String instrumentType;
    private int quantity;
    private BigDecimal price;
    private OffsetDateTime date;

}
