package com.example.stockapiapplication.service;

import com.example.stockapiapplication.model.Account;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TinkoffInvestApiService {
    private final String apiUrl = "https://api-invest.tinkoff.ru/openapi/";
    private final String token = "Ваш токен";
    private final RestTemplate restTemplate;

    public TinkoffInvestApiService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public Account getAccountInfo() {
        String url = apiUrl + "user/accounts";
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<Account> response = restTemplate.exchange(url, HttpMethod.GET, entity, Account.class);
        return response.getBody();
    }
}
