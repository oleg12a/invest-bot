package com.example.stockapiapplication.service;

import com.example.stockapiapplication.dto.StocksDto;
import com.example.stockapiapplication.dto.TickersDto;
import com.example.stockapiapplication.model.Account;
import com.example.stockapiapplication.model.Currency;
import com.example.stockapiapplication.model.Instrument;
import com.example.stockapiapplication.model.Stock;

import java.util.List;

public interface StockService {

    StocksDto getStocksByTickers(TickersDto tickers);

    void buyStock(String accountId, String figi, int quantity);

    void sellStock(String accountId, String figi, int quantity);
    List<Instrument> getInstruments(Currency currency);

    Stock getStockByFigi(String figi);

    Stock getStockByTicker(String ticker);
    List<Account> getAccounts();
}
