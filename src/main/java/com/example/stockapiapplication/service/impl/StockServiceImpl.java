package com.example.stockapiapplication.service.impl;

import com.example.stockapiapplication.dto.StocksDto;
import com.example.stockapiapplication.dto.TickersDto;
import com.example.stockapiapplication.model.Account;
import com.example.stockapiapplication.model.Currency;
import com.example.stockapiapplication.model.Instrument;
import com.example.stockapiapplication.model.Stock;
import com.example.stockapiapplication.service.StockService;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;
import ru.tinkoff.piapi.core.InstrumentsService;
import ru.tinkoff.piapi.core.InvestApi;

import java.util.List;


@Service
@RequiredArgsConstructor
public class StockServiceImpl implements StockService {

    private final InvestApi investApi;
    private InstrumentsService instrumentsService;

    @Override
    public StocksDto getStocksByTickers(TickersDto tickers) {
        return null;
    }

    @Override
    public void buyStock(String accountId, String figi, int quantity) {

    }

    @Override
    public void sellStock(String accountId, String figi, int quantity) {

    }

    @Override
    public List<Instrument> getInstruments(Currency currency) {
        return null;
    }

    @Override
    public Stock getStockByFigi(String figi) {
        return null;
    }

    @Override
    public Stock getStockByTicker(String ticker) {
        return null;
    }

    @Override
    public List<Account> getAccounts() {
        return null;
    }
}
