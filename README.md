# invest-bot




***

# Идентификация торговых инструментов

Для точного определения различных торговых инструментов используются различные идентификаторы:

   - Isin(англ.: International Security Identification Number) — международный идентификационный номер ценной бумаги, 
   состоит из 12 символов цифр и латинских букв, которые начинаются, 
   как правило, с 2-буквенного кода страны эмитента ценной бумаги.
   - Структура ISIN:
   - Первые два символа — буквы, определяющие код страны эмитента согласно стандарту ISO 3166-1 
   (например, российские ISIN-коды начинаются буквами RU).
   - Далее следуют 9 букв и цифр национального идентификационного кода ценной бумаги 
   (англ. National Securities Identifying Number, NSIN). Завершает код контрольная цифра.
   - Например, ISIN акции TCS Group — US87238U2033.

   - Ticker — краткое (1-5 букв) наименование ценной бумаги на конкретной бирже. 
   Без указания биржи и режима торгов, по сути, является бессмысленным набором букв. 
   Для этого на российских биржах MOEX и SPBE используется специальный признак "Режим торгов", который соответствует техническому термину "class_сode". 
   Комбинация тикера и "class_code" является уникальным идентификатором. Отдельно же, только тикер, - нет.
   - Например, тикер TCS Group Tinkoff - TCSG.

   - FIGI(англ.: Financial Instrument Global Identifier) — глобальный идентификатор финансового инструмента. 
   Представляет собой 12-символьный код из латинских букв и цифр, определяется как идентификатор 
   ценной бумаги на торговой площадке (бирже), которая является некоторым "источником цен".

Важно: не всегда FIGI, присваемые в Тинькофф Инвестициях, совпадают с международным классификатором. Является устаревшим параметром и не рекомендуется к использованию.

## Name
Invest Api .

Определения классов model:

Instrument: 
- Этот класс представляет собой финансовый инструмент с полями, такими как id, figi, isin, ticker, currency, name, increment и lot.

Operation: 
- Этот класс представляет собой операцию с полями, такими как id, status, operationType, currency, payment, commission, figi, instrumentType, quantity, price и date.

OperationType: 
- Это перечисление (enum), которое определяет тип операции: ПОКУПКА (BUY) или ПРОДАЖА (SELL).

InstrumentType: 
- Это перечисление (enum), которое определяет тип инструмента: АКЦИЯ (STOCK), ETF, ОБЛИГАЦИЯ (BOND) или ВАЛЮТА (CURRENCY).

Stock: 
- Этот класс представляет собой акцию с полями, такими как ticker, figi, name, type, currency и source. Этот класс является неизменяемым, что означает, что после создания его состояние не может быть изменено.

Account: 
- Этот класс представляет собой учетную запись с полями, такими как accountId, brokerAccountType, currency и getBrokerAccountId.

## Description
  - public List<Account> getAccounts() - метод, который возвращает список счетов пользователя
  - public List<Instrument> getInstruments(Currency currency) - метод, который возвращает список инструментов по заданной валюте
  - public List<Operation> getOperations(String accountId, OffsetDateTime from, OffsetDateTime to) - метод, который возвращает список операций по заданному счету и периоду времени
  - public Stock getStockByTicker(String ticker) - метод, который возвращает информацию об акции по заданному тикеру
  - public Stock getStockByFigi(String figi) - метод, который возвращает информацию об акции по заданному FIGI
  - public void buyStock(String accountId, String figi, int quantity) - метод, который покупает заданное количество акций по заданному FIGI на заданном счету
  - public void sellStock(String accountId, String figi, int quantity) - метод, который продает заданное количество акций по заданному FIGI на заданном счету

## MoneyValue
Данный тип данных используется для параметров, имеющих денежный эквивалент. Например, стоимость ценных бумаг.

Тип состоит из трёх параметров:

    currency — строковый ISO-код валюты. Например, RUB или USD.
    units — целая часть суммы.
    nano — дробная часть суммы (миллиардные доли).

Для корректной работы с данным типом данных требуется его конвертация в стандартные типы языка программирования, который используется для написания торгового робота.
Пример конвертации на Java
```
{
// MoneyValue - конвертация из BigDecimal в MoneyValue и обратно
    BigDecimal value = new BigDecimal("123.456");
    String currency = "RUB";
    MoneyValue moneyValue = MoneyValue.newBuilder() 
        .setCurrency(currency)
        .setUnits(value != null ? value.longValue() : 0)
        .setNano(value != null ? value.remainder(BigDecimal.ONE).multiply(BigDecimal
        .valueOf(1_000_000_000)).intValue() : 0)
        .build();
    BigDecimal bigDecimal = moneyValue.getUnits() == 0 && moneyValue.getNano() == 0 ? BigDecimal.ZERO:
      BigDecimal.valueOf(moneyValue.getUnits()).add(BigDecimal.valueOf(moneyValue.getNano(), 9)); 
}
```

## Quotation
Данный тип данных аналогичен MoneyValue с той лишь разницей, что в нём не содержится информации о валюте.

    units — целая часть суммы.
    nano — дробная часть суммы.

Пример конвертации на Java
```
{
// Пример конвертации из BigDecimal в Quotation и обратно
    BigDecimal value = new BigDecimal("123.456");
    Quotation quotation = Quotation.newBuilder()
        .setUnits(value != null ? value.longValue() : 0)
        .setNano(value != null ? value.remainder(BigDecimal.ONE)
        .multiply(BigDecimal.valueOf(1_000_000_000))
        .intValue() : 0)
        .build();
    BigDecimal bigDecimal = quotation.getUnits() == 0 && quotation.getNano() == 0 ? BigDecimal.ZERO :
     BigDecimal.valueOf(quotation.getUnits()).add(BigDecimal.valueOf(quotation.getNano(), 9));
}
```
